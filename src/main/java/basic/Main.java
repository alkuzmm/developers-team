package basic;

import basic.db.connection.DataSourceFactory;
import basic.db.dao.impl.DeveloperDao;
import basic.db.dao.impl.PositionDao;
import basic.db.dao.impl.ProjectDao;
import basic.db.dao.impl.TaskDao;
import basic.db.entity.Developer;
import basic.db.entity.Position;
import basic.db.entity.Project;
import basic.db.entity.Task;
import org.h2.tools.RunScript;

import javax.sql.DataSource;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;

public class Main {

    public static void main(String[] args) throws IOException, SQLException {

        DataSource H2DataSource = DataSourceFactory.H2.configuration.configure();


        RunScript.execute(H2DataSource.getConnection(),
                new FileReader("./src/main/resources/create_db.sql"));


        ProjectDao projectDao = new ProjectDao(H2DataSource);
        TaskDao taskDao = new TaskDao(H2DataSource);
        PositionDao positionDao = new PositionDao(H2DataSource);
        DeveloperDao developerDao = new DeveloperDao(H2DataSource);

        Project project = Project.builder()
                .name("Developers Team")
                .description("Реализовать систему Developers team")
                .build();

        Project projectCreated = projectDao.create(project);

        Task task = Task.builder()
                .level(1)
                .assigned(false)
                .description("Task description")
                .projectId(projectCreated.getId())
                .build();

        Task taskCreated = taskDao.create(task);

        Position position = Position.builder()
                .name("Developer")
                .level("MIDDLE")
                .build();

        Position positionCreated = positionDao.create(position);

        Developer developer = Developer.builder()
                .name("Tom")
                .free(false)
                .positionId(positionCreated.getId())
                .taskId(taskCreated.getId())
                .build();

        developerDao.create(developer);
        System.out.println("\n\n");
        System.out.println(projectDao.get(1));
        System.out.println(taskDao.get(1));
        System.out.println(positionDao.get(1));
        System.out.println(developerDao.get(1));

    }
}
