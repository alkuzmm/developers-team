package basic.db.dao.impl;


import basic.db.dao.BaseDao;
import basic.db.entity.Task;
import basic.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static basic.db.dao.impl.TaskDao.Table.*;


public class TaskDao implements BaseDao<Task> {

    private static final Logger LOGGER = LoggerFactory.getLogger(TaskDao.class);
    private static final String MAIN_EXCEPTION_MESSAGE = "Exception occurred";

    private DataSource dataSource;

    public TaskDao(final DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Task create(Task task) {

        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(INSERT_SQL, new String[]{"ID"})) {

            ps.setInt(1, task.getLevel());
            ps.setBoolean(2, task.isAssigned());
            ps.setString(3, task.getDescription());
            ps.setLong(4, task.getProjectId());

            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new DAOException("Error creating project");
            }
            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {

                if (generatedKeys.next()) {
                    final long id = generatedKeys.getLong(1);
                    return new Task(id, task);

                } else {
                    throw new DAOException("Could not create project");
                }
            }
        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public void create(List<Task> list) {

        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false); //true

            ps = conn.prepareStatement(INSERT_SQL, new String[]{"ID"});

            for (Task task : list) {

                ps.setInt(1, task.getLevel());
                ps.setBoolean(2, task.isAssigned());
                ps.setString(3, task.getDescription());
                ps.setLong(4, task.getProjectId());

                ps.addBatch();
            }

            ps.executeBatch();
            conn.commit();

            LOGGER.info("Prepared statement batch insert successfully");

        } catch (SQLException e) {
            try {
                if (conn != null) {
                    conn.rollback();
                }
            } catch (SQLException ex) {
                LOGGER.error("Error during rollback ", ex);
            }
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                LOGGER.error("Error during closing connection", e);
            }
        }
    }

    @Override
    public Task get(long id) {

        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(SELECT_ONE_SQL)) {
            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return toTask(rs);
                } else {
                    return null;
                }
            }
        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public List<Task> getAll() {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement ps = conn.prepareStatement(SELECT_ALL_SQL);
             ResultSet rs = ps.executeQuery()) {

            final List<Task> projects = new ArrayList<>();
            while (rs.next()) {
                projects.add(toTask(rs));
            }
            return projects;
        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public boolean update(Task task) {

        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(UPDATE_SQL)) {

            ps.setInt(1, task.getLevel());
            ps.setBoolean(2, task.isAssigned());
            ps.setString(3, task.getDescription());
            ps.setLong(4, task.getProjectId());
            ps.setLong(5, task.getId());

            final int rowCount = ps.executeUpdate();
            return rowCount != 0;

        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public boolean delete(long id) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(DELETE_SQL)) {
            ps.setLong(1, id);

            final int rowCount = ps.executeUpdate();
            return rowCount != 0;

        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    private Task toTask(ResultSet rs) throws SQLException {
        return Task.builder()
                .id(rs.getLong(ID))
                .level(rs.getInt(LEVEL))
                .assigned(rs.getBoolean(ASSIGNED))
                .description(rs.getString(DESCRIPTION))
                .projectId(rs.getLong(PROJECT_ID))
                .build();
    }

    interface Table {

        String ID = "ID";
        String LEVEL = "LEVEL";
        String ASSIGNED = "ASSIGNED";
        String DESCRIPTION = "DESCRIPTION";
        String PROJECT_ID = "PROJECT_ID";

        String INSERT_SQL = "INSERT INTO TASK (LEVEL, ASSIGNED, DESCRIPTION, PROJECT_ID) VALUES (?, ?, ?, ?)";
        String SELECT_ONE_SQL = "SELECT * FROM TASK WHERE ID = ?";
        String SELECT_ALL_SQL = "SELECT * FROM TASK";
        String UPDATE_SQL = "UPDATE TASK SET LEVEL = ?, ASSIGNED = ?, DESCRIPTION = ?, PROJECT_ID = ? WHERE ID = ?";
        String DELETE_SQL = "DELETE FROM TASK WHERE ID = ?";
    }
}
