package basic.db.dao.impl;


import basic.db.dao.BaseDao;
import basic.db.entity.Position;
import basic.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static basic.db.dao.impl.PositionDao.Table.*;

public class PositionDao implements BaseDao<Position> {

    private static final Logger LOGGER = LoggerFactory.getLogger(PositionDao.class);
    private static final String MAIN_EXCEPTION_MESSAGE = "Exception occurred";

    private DataSource dataSource;

    public PositionDao(final DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Position create(Position position) {

        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(INSERT_SQL, new String[]{"ID"})) {

            ps.setString(1, position.getName());
            ps.setString(2, position.getLevel());

            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new DAOException("Error creating position");
            }
            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {

                if (generatedKeys.next()) {
                    final long id = generatedKeys.getLong(1);
                    return new Position(id, position);

                } else {
                    throw new DAOException("Could not create position");
                }
            }
        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public void create(List<Position> list) {

        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false); //true

            ps = conn.prepareStatement(INSERT_SQL, new String[]{"ID"});

            for (Position position : list) {

                ps.setString(1, position.getName());
                ps.addBatch();
            }

            ps.executeBatch();
            conn.commit();

            LOGGER.info("Prepared statement batch insert successfully");

        } catch (SQLException e) {
            try {
                if (conn != null) {
                    conn.rollback();
                }
            } catch (SQLException ex) {
                LOGGER.error("Error during rollback ", ex);
            }
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                LOGGER.error("Error during closing connection", e);
            }
        }
    }

    @Override
    public Position get(long id) {

        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(SELECT_ONE_SQL)) {
            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return toPosition(rs);
                } else {
                    return null;
                }
            }
        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public List<Position> getAll() {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement ps = conn.prepareStatement(SELECT_ALL_SQL);
             ResultSet rs = ps.executeQuery()) {

            final List<Position> projects = new ArrayList<>();
            while (rs.next()) {
                projects.add(toPosition(rs));
            }
            return projects;
        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public boolean update(Position position) {

        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(UPDATE_SQL)) {

            ps.setString(1, position.getName());
            ps.setLong(2, position.getId());

            final int rowCount = ps.executeUpdate();
            return rowCount != 0;

        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public boolean delete(long id) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(DELETE_SQL)) {
            ps.setLong(1, id);

            final int rowCount = ps.executeUpdate();
            return rowCount != 0;

        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    Position toPosition(ResultSet rs) throws SQLException {
        return Position.builder()
                .id(rs.getLong(ID))
                .name(rs.getString(NAME))
                .level(rs.getString(LEVEL))
                .build();
    }

    interface Table {

        String ID = "ID";
        String NAME = "NAME";
        String LEVEL = "LEVEL";

        String INSERT_SQL = "INSERT INTO POSITION (NAME, LEVEL) VALUES (?, ?)";
        String UPDATE_SQL = "UPDATE POSITION SET NAME = ? WHERE ID = ?";
        String SELECT_ALL_SQL = "SELECT * FROM POSITION";
        String SELECT_ONE_SQL = "SELECT * FROM POSITION WHERE ID = ?";
        String DELETE_SQL = "DELETE FROM POSITION WHERE ID = ?";
    }
}
