package basic.db.dao.impl;


import basic.db.dao.BaseDao;
import basic.db.entity.Developer;
import basic.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static basic.db.dao.impl.DeveloperDao.Table.*;

public class DeveloperDao implements BaseDao<Developer> {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeveloperDao.class);
    private static final String MAIN_EXCEPTION_MESSAGE = "Exception occurred";

    private DataSource dataSource;

    public DeveloperDao(final DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Developer create(Developer developer) {

        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(INSERT_SQL, new String[]{"ID"})) {

            ps.setString(1, developer.getName());
            ps.setBoolean(2, developer.isFree());
            ps.setLong(3, developer.getPositionId());
            ps.setLong(4, developer.getTaskId());

            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new DAOException("Error creating developer");
            }
            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {

                if (generatedKeys.next()) {
                    final long id = generatedKeys.getLong(1);
                    return new Developer(id, developer);

                } else {
                    throw new DAOException("Could not create developer");
                }
            }
        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public void create(List<Developer> list) {

        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false); //true

            ps = conn.prepareStatement(INSERT_SQL, new String[]{"ID"});

            for (Developer developer : list) {

                ps.setString(1, developer.getName());
                ps.setBoolean(2, developer.isFree());
                ps.setLong(3, developer.getPositionId());
                ps.setLong(4, developer.getTaskId());

                ps.addBatch();
            }

            ps.executeBatch();
            conn.commit();

            LOGGER.info("Prepared statement batch insert successfully");

        } catch (SQLException e) {
            try {
                if (conn != null) {
                    conn.rollback();
                }
            } catch (SQLException ex) {
                LOGGER.error("Error during rollback ", ex);
            }
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                LOGGER.error("Error during closing connection", e);
            }
        }
    }

    @Override
    public Developer get(long id) {

        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(SELECT_ONE_SQL)) {
            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return toDeveloper(rs);
                } else {
                    return null;
                }
            }
        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public List<Developer> getAll() {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement ps = conn.prepareStatement(SELECT_ALL_SQL);
             ResultSet rs = ps.executeQuery()) {

            final List<Developer> projects = new ArrayList<>();
            while (rs.next()) {
                projects.add(toDeveloper(rs));
            }
            return projects;
        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public boolean update(Developer developer) {

        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(UPDATE_SQL)) {

            ps.setString(1, developer.getName());
            ps.setBoolean(2, developer.isFree());
            ps.setLong(3, developer.getPositionId());
            ps.setLong(4, developer.getTaskId());
            ps.setLong(5, developer.getId());

            final int rowCount = ps.executeUpdate();
            return rowCount != 0;

        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public boolean delete(long id) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(DELETE_SQL)) {
            ps.setLong(1, id);

            final int rowCount = ps.executeUpdate();
            return rowCount != 0;

        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    private Developer toDeveloper(ResultSet rs) throws SQLException {
        return Developer.builder()
                .id(rs.getLong(ID))
                .name(rs.getString(NAME))
                .free(rs.getBoolean(FREE))
                .positionId(rs.getLong(POSITION_ID))
                .taskId(rs.getLong(TASK_ID))
                .build();
    }

    interface Table {

        String ID = "ID";
        String NAME = "NAME";
        String FREE = "FREE";
        String POSITION_ID = "POSITION_ID";
        String TASK_ID = "TASK_ID";

        String INSERT_SQL = "INSERT INTO DEVELOPER (NAME, FREE, POSITION_ID, TASK_ID) VALUES (?, ?, ?, ?)";
        String SELECT_ONE_SQL = "SELECT * FROM DEVELOPER WHERE ID = ?";
        String SELECT_ALL_SQL = "SELECT * FROM DEVELOPER";
        String UPDATE_SQL = "UPDATE DEVELOPER SET NAME = ?, FREE = ?, POSITION_ID = ?, TASK_ID = ? WHERE ID = ?";
        String DELETE_SQL = "DELETE FROM DEVELOPER WHERE ID = ?";
    }
}
