package basic.db.dao.impl;

import basic.exception.DAOException;
import basic.db.dao.BaseDao;
import basic.db.entity.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static basic.db.dao.impl.ProjectDao.Table.*;

public class ProjectDao implements BaseDao<Project> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectDao.class);
    private static final String MAIN_EXCEPTION_MESSAGE = "Exception occurred";

    private DataSource dataSource;

    public ProjectDao(final DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Project create(Project project) {

        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(INSERT_SQL, new String[]{"ID"})) {

            ps.setString(1, project.getName());
            ps.setString(2, project.getDescription());

            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new DAOException("Error creating project");
            }
            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {

                if (generatedKeys.next()) {
                    final long id = generatedKeys.getLong(1);
                    return new Project(id, project);

                } else {
                    throw new DAOException("Could not create project");
                }
            }
        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public void create(List<Project> list) {

        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false); //true

            ps = conn.prepareStatement(INSERT_SQL, new String[]{"ID"});

            for (Project project : list) {

                ps.setString(1, project.getName());
                ps.setString(2, project.getDescription());

                ps.addBatch();
            }

            ps.executeBatch();
            conn.commit();

            LOGGER.info("Prepared statement batch insert successfully");

        } catch (SQLException e) {
            try {
                if (conn != null) {
                    conn.rollback();
                }
            } catch (SQLException ex) {
                LOGGER.error("Error during rollback ", ex);
            }
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                LOGGER.error("Error during closing connection", e);
            }
        }
    }

    @Override
    public Project get(long id) {

        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(SELECT_ONE_SQL)) {
            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return toProject(rs);
                } else {
                    return null;
                }
            }
        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public List<Project> getAll() {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement ps = conn.prepareStatement(SELECT_ALL_SQL);
             ResultSet rs = ps.executeQuery()) {

            final List<Project> projects = new ArrayList<>();
            while (rs.next()) {
                projects.add(toProject(rs));
            }
            return projects;
        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public boolean update(Project project) {

        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(UPDATE_SQL)) {

            ps.setString(1, project.getName());
            ps.setString(2, project.getDescription());
            ps.setLong(3, project.getId());

            final int rowCount = ps.executeUpdate();
            return rowCount != 0;

        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public boolean delete(long id) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(DELETE_SQL)) {
            ps.setLong(1, id);

            final int rowCount = ps.executeUpdate();
            return rowCount != 0;

        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    private Project toProject(ResultSet rs) throws SQLException {
        return Project.builder()
                .id(rs.getLong(ID))
                .name(rs.getString(NAME))
                .description(rs.getString(DESCRIPTION))
                .build();
    }

    interface Table {

        String ID = "ID";
        String NAME = "NAME";
        String DESCRIPTION = "DESCRIPTION";

        String INSERT_SQL = "INSERT INTO PROJECT (NAME, DESCRIPTION) VALUES (?, ?)";
        String UPDATE_SQL = "UPDATE PROJECT SET NAME = ?, DESCRIPTION = ? WHERE ID = ?";
        String SELECT_ALL_SQL = "SELECT * FROM PROJECT";
        String SELECT_ONE_SQL = "SELECT * FROM PROJECT WHERE ID = ?";
        String DELETE_SQL = "DELETE FROM PROJECT WHERE ID = ?";
    }
}
