package basic.db.dao;

import java.util.List;

public interface BaseDao<T> {

    T create(T t);

    void create(List<T> list);

    T get(long id);

    List<T> getAll();

    boolean update(T t);

    boolean delete(long id);
}