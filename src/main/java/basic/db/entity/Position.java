package basic.db.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Position {

    private long id;

    private String name;

    private String level;

    public Position(long id, Position position) {
        this.id = id;
        this.name = position.getName();
        this.level = position.getLevel();
    }
}
