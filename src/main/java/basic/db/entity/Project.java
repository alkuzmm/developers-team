package basic.db.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
//@AllArgsConstructor
public class Project {

    private long id;

    private String name;

    private String description;


    public Project(long id, Project project) {
        this.id = id;
        this.name = project.getName();
        this.description = project.getDescription();
    }

    public Project(long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

}
