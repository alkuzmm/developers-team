package basic.db.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Developer {

    private long id;

    private String name;

    private boolean free;

    private long positionId;

    private long taskId;

    public Developer(long id, Developer developer) {
        this.id = id;
        this.name = developer.getName();
        this.free = developer.isFree();
        this.positionId = developer.getPositionId();
        this.taskId = developer.getTaskId();
    }
}
