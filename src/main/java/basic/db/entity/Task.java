package basic.db.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Task {

    private long id;

    private int level;

    private boolean assigned;

    private String description;

    private long projectId;

    public Task(long id, Task task) {
        this.id = id;
        this.level = task.getLevel();
        this.assigned = task.isAssigned();
        this.description = task.getDescription();
        this.projectId = task.getProjectId();
    }

    public Task(long id, int level, String description, long projectId) {
        this.id = id;
        this.level = level;
        this.description = description;
        this.projectId = projectId;
    }
}
