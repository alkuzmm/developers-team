package db.dao;

import basic.db.dao.impl.TaskDao;
import basic.db.entity.Task;
import basic.exception.DAOException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mockito;

import javax.sql.DataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class TaskDaoTest {

    String ID = "ID";
    int LEVEL = 1;
    boolean ASSIGNED = false;
    String DESCRIPTION = "task description";
    long PROJECT_ID = 1L;


    String INSERT_SQL = "INSERT INTO TASK (LEVEL, ASSIGNED, DESCRIPTION, PROJECT_ID) VALUES (?, ?, ?, ?)";
    String SELECT_ONE_SQL = "SELECT * FROM TASK WHERE ID = ?";
    String SELECT_ALL_SQL = "SELECT * FROM TASK";
    String UPDATE_SQL = "UPDATE TASK SET LEVEL = ?, ASSIGNED = ?, DESCRIPTION = ?, PROJECT_ID = ? WHERE ID = ?";
    String DELETE_SQL = "DELETE FROM TASK WHERE ID = ?";

    //expected
    private static final long TEST_ID = 1L;
    private static final long TEST_ID_PROJECT = 1L;
    private static final Task TEST_TASK_EXPECTED = new Task(
            TEST_ID, 1, "task description", TEST_ID_PROJECT);

    private DataSource mockDataSource = mock(DataSource.class);
    private Connection mockConnection = mock(Connection.class);
    private PreparedStatement mockPreparedStatement = mock(PreparedStatement.class);
    private ResultSet mockResultSet = mock(ResultSet.class);

    private TaskDao taskDao = new TaskDao(mockDataSource);

    @BeforeEach
    void before() throws Exception {
        when(mockDataSource.getConnection()).thenReturn(mockConnection);
        when(mockPreparedStatement.executeQuery()).thenReturn(mockResultSet);
        when(mockPreparedStatement.getGeneratedKeys()).thenReturn(mockResultSet);
    }


    @Test
    void whenCreateTaskShouldReturnTask() throws Exception {
        //given
        when(mockConnection.prepareStatement(INSERT_SQL, new String[]{ID})).thenReturn(mockPreparedStatement);
        when(mockResultSet.next()).thenReturn(true);
        when(mockPreparedStatement.executeUpdate()).thenReturn(1);

        when(mockResultSet.getLong(1)).thenReturn(TEST_ID); //*

        //when
        Task actual = taskDao.create(buildTestTask());

        //then
        assertEquals(TEST_TASK_EXPECTED, actual);
    }

    @Test
    void whenCreateTaskShouldThrowExceptionIfIdWasNotGenerated() throws Exception {
        //given
        when(mockConnection.prepareStatement(INSERT_SQL, new String[]{ID})).thenReturn(mockPreparedStatement);

        //when
        final Executable executable = () -> taskDao.create(buildTestTask());

        //then
        assertThrows(DAOException.class, executable);
    }

    @Test
    void whenCreateTaskWithoutGeneratedKeyShouldBeException() throws Exception {

        // given
        when(mockConnection.prepareStatement(INSERT_SQL, new String[]{ID})).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeUpdate()).thenReturn(1);
        when(mockResultSet.next()).thenReturn(false);

        // when
        var expectedException = assertThrows(DAOException.class, () -> taskDao.create(buildTestTask()));

        // then
        assertEquals("Could not create project", expectedException.getMessage());
    }

    @Test
    void whenCreateTaskErrorShouldBeException() throws Exception {

        // given
        when(mockConnection.prepareStatement(INSERT_SQL, new String[]{ID})).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeUpdate()).thenThrow(new SQLException("Exception occurred"));

        // when
        var expectedException = assertThrows(DAOException.class, () -> taskDao.create(buildTestTask()));

        // then
        assertEquals("java.sql.SQLException: Exception occurred", expectedException.getMessage());
    }

    @Test
    void whenCreateTaskFromListShouldBeSuccess() throws Exception {

        // qiven
        when(mockConnection.prepareStatement(INSERT_SQL, new String[]{ID})).thenReturn(mockPreparedStatement);

        // then
        assertDoesNotThrow(() -> taskDao.create(buildTestListTasks()));
    }

    @Test
    void whenCreateTaskFromListNotSuccessfulSholdBeRollBack() throws Exception {

        // given
        when(mockConnection.prepareStatement(INSERT_SQL, new String[]{ID})).thenReturn(mockPreparedStatement);
        doThrow(new SQLException()).when(mockConnection).commit();

        // when
        assertDoesNotThrow(() -> taskDao.create(buildTestListTasks()));

        // then
        verify(mockConnection).rollback();
    }

    @Test
    void whenGetTaskShouldReturnTask() throws Exception {

        // given
        when(mockConnection.prepareStatement(SELECT_ONE_SQL)).thenReturn(mockPreparedStatement);
        when(mockResultSet.next()).thenReturn(true);
        when(mockResultSet.getLong("ID")).thenReturn(TEST_TASK_EXPECTED.getId());
        when(mockResultSet.getInt("LEVEL")).thenReturn(TEST_TASK_EXPECTED.getLevel());
        when(mockResultSet.getBoolean("ASSIGNED")).thenReturn(TEST_TASK_EXPECTED.isAssigned());
        when(mockResultSet.getString("DESCRIPTION")).thenReturn(TEST_TASK_EXPECTED.getDescription());
        when(mockResultSet.getLong("PROJECT_ID")).thenReturn(TEST_TASK_EXPECTED.getProjectId());

        // when
        Task actual = taskDao.get(TEST_ID);

        // then
        assertEquals(TEST_TASK_EXPECTED, actual);

    }

    @Test
    void whenGetTaskShouldReturnNull() throws Exception {

        // given
        when(mockConnection.prepareStatement(SELECT_ONE_SQL)).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeQuery()).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(false);

        // when
        Task actual = taskDao.get(TEST_ID);

        // then
        assertNull(actual);
    }

    @Test
    void whenGetTaskShouldBeException() throws Exception {

        // given
        when(mockConnection.prepareStatement(SELECT_ONE_SQL)).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeQuery()).thenThrow(new SQLException("Exception text"));

        // when
        var expectedException = assertThrows(DAOException.class, () -> taskDao.get(TEST_ID));

        // then
        assertEquals("java.sql.SQLException: Exception text", expectedException.getMessage());
    }

    @Test
    void whenGetAllTaskShouldBeReturnTaskList() throws Exception {

        // given
        when(mockConnection.prepareStatement(SELECT_ALL_SQL)).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeQuery()).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(true, false);
        when(mockResultSet.getLong("ID")).thenReturn(TEST_TASK_EXPECTED.getId());
        when(mockResultSet.getInt("LEVEL")).thenReturn(TEST_TASK_EXPECTED.getLevel());
        when(mockResultSet.getBoolean("ASSIGNED")).thenReturn(TEST_TASK_EXPECTED.isAssigned());
        when(mockResultSet.getString("DESCRIPTION")).thenReturn(TEST_TASK_EXPECTED.getDescription());
        when(mockResultSet.getLong("PROJECT_ID")).thenReturn(TEST_TASK_EXPECTED.getProjectId());

        // when
        List<Task> tasks = taskDao.getAll();

        // then
        assertEquals(1, tasks.size());
    }

    @Test
    void whenGetAllTaskListShouldBeReturnException() throws Exception {

        // given
        when(mockConnection.prepareStatement(SELECT_ALL_SQL)).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeQuery()).thenThrow(new SQLException("Exception text"));

        // when
        var expectedException = assertThrows(DAOException.class, () -> taskDao.getAll());

        // then
        assertEquals("java.sql.SQLException: Exception text", expectedException.getMessage());
    }

    @Test
    void whenUpdateTaskShouldBeReturnTrue() throws Exception {

        // given
        when(mockConnection.prepareStatement(UPDATE_SQL)).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeUpdate()).thenReturn(1);

        // when
        boolean result = taskDao.update(TEST_TASK_EXPECTED);

        //then
        assertTrue(result);
    }

    @Test
    void whenUpdateTaskShouldBeReturnFalse() throws Exception {

        // given
        when(mockConnection.prepareStatement(UPDATE_SQL)).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeUpdate()).thenReturn(0);

        // when
        boolean result = taskDao.update(TEST_TASK_EXPECTED);

        //then
        assertFalse(result);
    }

    @Test
    void whenUpdateTaskShouldBeException() throws Exception {

        // given
        when(mockConnection.prepareStatement(UPDATE_SQL)).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeUpdate()).thenThrow(new SQLException("Exception text"));

        // when
        var expectedException = assertThrows(DAOException.class, () -> taskDao.update(TEST_TASK_EXPECTED));

        //then
        assertEquals("java.sql.SQLException: Exception text", expectedException.getMessage());
    }

    @Test
    void whenDeleteTaskShouldBeTrue() throws Exception {

        // given
        when(mockConnection.prepareStatement(DELETE_SQL)).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeUpdate()).thenReturn(1);

        // when
        boolean result = taskDao.delete(TEST_ID);

        //then
        assertTrue(result);
    }

    @Test
    void whenDeleteTaskShouldBeFalse() throws Exception {

        // given
        when(mockConnection.prepareStatement(DELETE_SQL)).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeUpdate()).thenReturn(0);

        // when
        boolean result = taskDao.delete(TEST_ID);

        //then
        assertFalse(result);
    }

    @Test
    void whenDeleteTaskShouldBeException() throws Exception {

        // given
        when(mockConnection.prepareStatement(DELETE_SQL)).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeUpdate()).thenThrow(new SQLException("Exception text"));

        // when
        var expectedException = assertThrows(DAOException.class, () -> taskDao.delete(TEST_ID));

        //then
        assertEquals("java.sql.SQLException: Exception text", expectedException.getMessage());
    }

    private Task buildTestTask() {
        return Task.builder()
                .level(LEVEL)
                .assigned(ASSIGNED)
                .description(DESCRIPTION)
                .projectId(PROJECT_ID)
                .build();
    }

    private List<Task> buildTestListTasks() {
        Task task1 = Mockito.spy(Task.builder()
                .level(1)
                .assigned(true)
                .description("one task description")
                .projectId(1)
                .build());
        Task task2 = Mockito.spy(Task.builder()
                .level(1)
                .assigned(true)
                .description("two task description")
                .projectId(1)
                .build());

        return List.of(task1, task2);
    }
}