package db.dao;

import basic.db.dao.impl.ProjectDao;
import basic.db.entity.Project;
import basic.exception.DAOException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ProjectDaoMockTest {
    //table
    String ID = "ID";
    String NAME = "NAME";
    String DESCRIPTION = "DESCRIPTION";

    //scripts
    String INSERT_SQL = "INSERT INTO PROJECT (NAME, DESCRIPTION) VALUES (?, ?)";
    String UPDATE_SQL = "UPDATE PROJECT SET NAME = ?, DESCRIPTION = ? WHERE ID = ?";
    String SELECT_ALL_SQL = "SELECT * FROM PROJECT";
    String SELECT_ONE_SQL = "SELECT * FROM PROJECT WHERE ID = ?";
    String DELETE_SQL = "DELETE FROM PROJECT WHERE ID = ?";

    //expected
    private static final long TEST_ID = 1L;
    private static final Project TEST_PROJECT_EXPECTED = new Project(
            TEST_ID, "project name", "project description");

    private DataSource mockDataSource = mock(DataSource.class);
    private Connection mockConnection = mock(Connection.class);
    private PreparedStatement mockPreparedStatement = mock(PreparedStatement.class);
    private ResultSet mockResultSet = mock(ResultSet.class);

    private ProjectDao projectDao = new ProjectDao(mockDataSource);

    @BeforeEach
    void before() throws Exception {
        when(mockDataSource.getConnection()).thenReturn(mockConnection);
        when(mockPreparedStatement.executeQuery()).thenReturn(mockResultSet);
        when(mockPreparedStatement.getGeneratedKeys()).thenReturn(mockResultSet);
    }

    @Test
    void whenCreateProjectShouldReturnProject() throws Exception {
        //given
        when(mockConnection.prepareStatement(INSERT_SQL, new String[]{ID})).thenReturn(mockPreparedStatement);
        when(mockResultSet.next()).thenReturn(true);
        when(mockPreparedStatement.executeUpdate()).thenReturn(1);

        when(mockResultSet.getLong(1)).thenReturn(TEST_ID); //*

        //when
        Project actual = projectDao.create(buildTestProject());

        //then
        assertEquals(TEST_PROJECT_EXPECTED, actual);
    }

    @Test
    void whenCreateProjectShouldThrowExceptionIfIdWasNotGenerated() throws Exception {
        //given
        when(mockConnection.prepareStatement(INSERT_SQL, new String[]{ID})).thenReturn(mockPreparedStatement);

        //when
        final Executable executable = () -> projectDao.create(buildTestProject());

        //then
        assertThrows(DAOException.class, executable);
    }

    @Test
    void whenCreateProjectWithoutGeneratedKeyShouldBeException() throws Exception {

        // given
        when(mockConnection.prepareStatement(INSERT_SQL, new String[]{ID})).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeUpdate()).thenReturn(1);
        when(mockResultSet.next()).thenReturn(false);

        // when
        var expectedException = assertThrows(DAOException.class, () -> projectDao.create(buildTestProject()));

        // then
        assertEquals("Could not create project", expectedException.getMessage());
    }

    @Test
    void whenCreateProjectErrorShouldBeException() throws Exception {

        // given
        when(mockConnection.prepareStatement(INSERT_SQL, new String[]{ID})).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeUpdate()).thenThrow(new SQLException("Exception occurred"));

        // when
        var expectedException = assertThrows(DAOException.class, () -> projectDao.create(buildTestProject()));

        // then
        assertEquals("java.sql.SQLException: Exception occurred", expectedException.getMessage());
    }

    @Test
    void whenCreateProjectFromListShouldBeSuccess() throws Exception {

        // qiven
        when(mockConnection.prepareStatement(INSERT_SQL, new String[]{ID})).thenReturn(mockPreparedStatement);

        // then
        assertDoesNotThrow(() -> projectDao.create(buildTestListProject()));
    }

    @Test
    void whenCreateProjectFromListNotSuccessfulSholdBeRollBack() throws Exception {

        // given
        when(mockConnection.prepareStatement(INSERT_SQL, new String[]{ID})).thenReturn(mockPreparedStatement);
        doThrow(new SQLException()).when(mockConnection).commit();

        // when
        assertDoesNotThrow(() -> projectDao.create(buildTestListProject()));

        // then
        verify(mockConnection).rollback();
    }

    @Test
    void whenGetProjectShouldReturnProject() throws Exception {

        // given
        when(mockConnection.prepareStatement(SELECT_ONE_SQL)).thenReturn(mockPreparedStatement);
        when(mockResultSet.next()).thenReturn(true);
        when(mockResultSet.getLong(ID)).thenReturn(TEST_PROJECT_EXPECTED.getId());
        when(mockResultSet.getString(NAME)).thenReturn(TEST_PROJECT_EXPECTED.getName());
        when(mockResultSet.getString(DESCRIPTION)).thenReturn(TEST_PROJECT_EXPECTED.getDescription());

        // when
        Project actual = projectDao.get(TEST_ID);

        // then
        assertEquals(TEST_PROJECT_EXPECTED, actual);
    }

    @Test
    void whenGetProjectShouldReturnNull() throws Exception {

        // given
        when(mockConnection.prepareStatement(SELECT_ONE_SQL)).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeQuery()).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(false);

        // when
        Project actual = projectDao.get(TEST_ID);

        // then
        assertNull(actual);
    }

    @Test
    void whenGetProjectShouldBeException() throws Exception {

        // given
        when(mockConnection.prepareStatement(SELECT_ONE_SQL)).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeQuery()).thenThrow(new SQLException("Exception text"));

        // when
        var expectedException = assertThrows(DAOException.class, () -> projectDao.get(TEST_ID));

        // then
        assertEquals("java.sql.SQLException: Exception text", expectedException.getMessage());
    }

    @Test
    void whenGetAllProjectListShouldBeReturnProjectList() throws Exception {

        // given
        when(mockConnection.prepareStatement(SELECT_ALL_SQL)).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeQuery()).thenReturn(mockResultSet);
        when(mockResultSet.next()).thenReturn(true, false);
        when(mockResultSet.getLong(ID)).thenReturn((TEST_PROJECT_EXPECTED.getId()));
        when(mockResultSet.getString(NAME)).thenReturn(TEST_PROJECT_EXPECTED.getName());
        when(mockResultSet.getString(DESCRIPTION)).thenReturn(TEST_PROJECT_EXPECTED.getDescription());

        // when
        List<Project> projects = projectDao.getAll();

        // then
        assertEquals(1, projects.size());
    }

    @Test
    void whenGetProjectListShouldBeReturnException() throws Exception {

        // given
        when(mockConnection.prepareStatement(SELECT_ALL_SQL)).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeQuery()).thenThrow(new SQLException("Exception text"));

        // when
        var expectedException = assertThrows(DAOException.class, () -> projectDao.getAll());

        // then
        assertEquals("java.sql.SQLException: Exception text", expectedException.getMessage());
    }

    @Test
    void whenUpdateProjectShouldBeReturnTrue() throws Exception {

        // given
        when(mockConnection.prepareStatement(UPDATE_SQL)).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeUpdate()).thenReturn(1);

        // when
        boolean result = projectDao.update(TEST_PROJECT_EXPECTED);

        //then
        assertTrue(result);
    }

    @Test
    void whenUpdateProjectShouldBeReturnFalse() throws Exception {

        // given
        when(mockConnection.prepareStatement(UPDATE_SQL)).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeUpdate()).thenReturn(0);

        // when
        boolean result = projectDao.update(TEST_PROJECT_EXPECTED);

        //then
        assertFalse(result);
    }

    @Test
    void whenUpdateProjectShouldBeException() throws Exception {

        // given
        when(mockConnection.prepareStatement(UPDATE_SQL)).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeUpdate()).thenThrow(new SQLException("Exception text"));

        // when
        var expectedException = assertThrows(DAOException.class, () -> projectDao.update(TEST_PROJECT_EXPECTED));

        //then
        assertEquals("java.sql.SQLException: Exception text", expectedException.getMessage());
    }

    @Test
    void whenDeleteProjectShouldBeTrue() throws Exception {

        // given
        when(mockConnection.prepareStatement(DELETE_SQL)).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeUpdate()).thenReturn(1);

        // when
        boolean result = projectDao.delete(TEST_ID);

        //then
        assertTrue(result);
    }

    @Test
    void whenDeleteProjectShouldBeFalse() throws Exception {

        // given
        when(mockConnection.prepareStatement(DELETE_SQL)).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeUpdate()).thenReturn(0);

        // when
        boolean result = projectDao.delete(TEST_ID);

        //then
        assertFalse(result);
    }

    @Test
    void whenDeleteProjectShouldBeException() throws Exception {

        // given
        when(mockConnection.prepareStatement(DELETE_SQL)).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeUpdate()).thenThrow(new SQLException("Exception text"));

        // when
        var expectedException = assertThrows(DAOException.class, () -> projectDao.delete(TEST_ID));

        //then
        assertEquals("java.sql.SQLException: Exception text", expectedException.getMessage());
    }

    private Project buildTestProject() {
        return Project.builder()
                .name("project name")
                .description("project description")
                .build();
    }

    private List<Project> buildTestListProject() {
        Project project1 = Mockito.spy(Project.builder()
                .name("ode project name")
                .description("one project description")
                .build());
        Project project2 = Mockito.spy(Project.builder()
                .name("two project name")
                .description("two project description")
                .build());
        return List.of(project1, project2);
    }
}
